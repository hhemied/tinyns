# TinyNS
The smallest dynamic dns for testing purpose, Wild card is supported
```
Master will be the latest and stable version
```


## Prerequisites
* docker
* docker-compose
* go

### How does it work?
* tinyns command will create the working directories, 
* Pull all you need,
* apply your configuration
* finally build a local docker images and build a docker-compose service to track the container

### Steps:
* go get gitlab.com/hhemied/tinyns || (or) under ~/go/src/gitlab.com/hhemied/ -> git clone https://gitlab.com/hhemied/tinyns.git
* cd ~/go/src/gitlab/hhemied/tinyns
* go install
* run tinyns command
* docker-compose down [for bind service]
* docker rmi localns
* EDIT  ~/.tinyns/config.toml
* run tinyns command

### To edit the resolvable names and IPs
* EDIT  ~/.tinyns/config.toml
* docker-compose down
* docker rmi localns
* run tinyns command
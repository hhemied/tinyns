#!/bin/bash

# Server version: Red Hat Enterprise Linux 7 and CentOS 7
# Using default firewalld

currentTimestamp=`date +%y-%m-%d-%H:%M:%S`

prefix=""


configFile=$prefix/etc/named.conf
configFileBackup=$configFile.backup.${currentTimestamp}
if [ -f $configFile ]; then
    echo backup $configFile $configFileBackup
    cp $configFile $configFileBackup
fi
echo "Write the configure to bind configuration file $configFile"
cat > $configFile <<EOF
options {

        listen-on port 53 { any; };
        listen-on-v6 port 53 { any; };
        directory       "/var/named";
        dump-file       "/var/named/data/cache_dump.db";
        statistics-file "/var/named/data/named_stats.txt";
        memstatistics-file "/var/named/data/named_mem_stats.txt";
        allow-query     { any; };
        allow-query-cache { any; };
        recursion yes;
        allow-transfer  { none; };
        forwarders {
		       1.1.1.1;
	      };
};
logging {
        channel default_debug {
                file "data/named.run";
                severity dynamic;
        };
};




zone "." IN {
        type hint;
        file "named.ca";
};

zone "lab.local" IN {
        type master;
        file "named.example0.com";
};

EOF
chown root:named $configFile

configFile=$prefix/var/named/named.ca
if [ ! -s $configFile ]; then
cat > $configFile <<EOF
EOF
chown root:named $configFile
fi


configFile=$prefix/var/named/named.example0.com
configFileBackup=$configFile.backup.${currentTimestamp}
if [ -f $configFile ]; then
    echo backup $configFile $configFileBackup
    cp $configFile $configFileBackup
fi
echo "Write RR to $configFile"
cat > $configFile <<EOF
\$TTL    600
@   IN SOA  ns.lab.local. root.mail.lab.local. (
                     2019020700   ; serial
                     1800  ; refresh
                     1800  ; retry
                     604800  ; expire
                     86400 )    ; minimum

@ IN NS ns.lab.local.
ns.lab.local. IN A {{ .LocalIP}}
{{ range $key, $value := .Records }}
{{ $value.Name }}.lab.local. IN A {{ $value.IP }} {{ end }}
EOF

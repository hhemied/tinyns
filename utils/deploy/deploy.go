package deploy

import (
	"bytes"
	"io"
	"log"
	"os"
	"os/exec"
	"path/filepath"
)

var wDir = filepath.Join(os.Getenv("HOME"), ".tinyns")

func DeployImage() {
	tty, err := os.OpenFile("/dev/ttys001", os.O_RDWR, os.ModePerm)
	if err != nil {
		log.Fatalln(err)
	}
	defer tty.Close()
	os.Chdir(wDir)

	cmd := exec.Command("docker", "build", ".", "-t", "localns")
	// cmd.Dir = dir
	var stdBuffer bytes.Buffer
	mw := io.MultiWriter(os.Stdout, &stdBuffer)

	cmd.Stdout = mw
	cmd.Stderr = mw

	// Execute the command
	if err := cmd.Run(); err != nil {
		log.Panic(err)
	}

	log.Println(stdBuffer.String())

}
func DeployService() {

	tty, err := os.OpenFile("/dev/ttys001", os.O_RDWR, os.ModePerm)
	if err != nil {
		log.Fatalln(err)
	}
	defer tty.Close()
	os.Chdir(wDir)
	cmd := exec.Command("docker-compose", "up", "-d")
	// cmd.Dir = dir
	var stdBuffer bytes.Buffer
	mw := io.MultiWriter(os.Stdout, &stdBuffer)

	cmd.Stdout = mw
	cmd.Stderr = mw

	// Execute the command
	if err := cmd.Run(); err != nil {
		log.Panic(err)
	}

	log.Println(stdBuffer.String())

	// Make BAckup
	cmd = exec.Command("cp", filepath.Join(wDir, "ns.sh.origin"), filepath.Join(wDir, "ns.sh"))
	// cmd.Dir = dir
	mwn := io.MultiWriter(os.Stdout, &stdBuffer)

	cmd.Stdout = mwn
	cmd.Stderr = mwn

	// Execute the command
	if err := cmd.Run(); err != nil {
		log.Panic(err)
	}

	log.Println(stdBuffer.String())

}

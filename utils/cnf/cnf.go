package cnf

import (
	"log"
	"os"
	"path/filepath"
	"text/template"

	"github.com/BurntSushi/toml"
)

const (
	f  = "config.toml"
	ns = "ns.sh"
)

var wDir = filepath.Join(os.Getenv("HOME"), ".tinyns")

type Config struct {
	LocalIP string
	Records map[string]Names
}

type Names struct {
	Name string
	IP   string
}

func LoadConfigFile(filename string) (*Config, error) {
	var config Config
	if _, err := toml.DecodeFile(filepath.Join(wDir, f), &config); err != nil {
		return nil, err
	}
	return &config, nil
}

func ConfigHandler() {
	c, err := LoadConfigFile(filepath.Join(wDir, f))
	if err != nil {
		log.Fatal(err)
	}
	to, err := os.OpenFile(filepath.Join(wDir, ns), os.O_RDWR|os.O_CREATE, 0754)
	if err != nil {
		log.Fatal(err)
	}
	t, err := template.ParseFiles(filepath.Join(wDir, ns))
	if err != nil {
		log.Fatal(err)
	}
	err = t.Execute(to, c)
	if err != nil {
		log.Fatal(err)
	}
	defer to.Close()
}

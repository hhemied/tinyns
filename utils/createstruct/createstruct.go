package createstruct

import (
	"os"
	"path/filepath"

	"github.com/fatih/color"
	cp "gitlab.com/tinyns/utils/copy"
)

var CFDir = filepath.Join(os.Getenv("HOME"), "go", "src", "gitlab.com", "tinyns")
var WDir = filepath.Join(os.Getenv("HOME"), ".tinyns")

// Build build the structure
func Build() {
	if _, err := os.Stat(CFDir); os.IsNotExist(err) {
		color.Red("TinyNS is not installed correctly..")
	}

	cp.CopyDir(CFDir, WDir)

}

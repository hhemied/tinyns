package main

import (
	cnf "gitlab.com/tinyns/utils/cnf"
	cs "gitlab.com/tinyns/utils/createstruct"
	deploy "gitlab.com/tinyns/utils/deploy"
)

func main() {
	cs.Build()
	cnf.ConfigHandler()
	deploy.DeployImage()
	deploy.DeployService()

}

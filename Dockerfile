FROM hhemied/tinyns:v0.0.3
LABEL Author="Hazem Hemied" Author_email="hazem.hemied@gmail.com"
COPY ns.sh /tmp
RUN sed -i '$d' /tmp/ns.sh ; sed -i '$d' /tmp/ns.sh && /bin/bash /tmp/ns.sh && systemctl enable named-chroot && rm -f /tmp/ns.sh
CMD ["/usr/sbin/init"]